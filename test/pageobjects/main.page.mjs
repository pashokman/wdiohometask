import Page from './page.mjs';

class MainPage extends Page {

    get textarea () { return $('#postform-text') };

    get spanOpenSelect () { return $('#select2-postform-expiration-container') };
    get ulLength () { return $('#select2-postform-expiration-results').$$('li').length };

    get pasteNameTitle () { return $('form input[id="postform-name"]') };

    async open () {
        await super.open('https://pastebin.com/');
    };

};

export default new MainPage();