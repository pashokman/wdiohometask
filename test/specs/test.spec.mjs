import mainPage from "../pageobjects/main.page.mjs";
import { Key } from 'webdriverio';

describe('Home work testing', () => {
    
    it('Code: "Hello from WebDriver"', async() => {
        mainPage.open();
        await mainPage.textarea.setValue('Hello from WebDriver');

        expect(await mainPage.textarea.getValue()).toEqual('Hello from WebDriver');
    });

    it('Paste Expiration: "10 Minutes"', async() => {
        await mainPage.spanOpenSelect.scrollIntoView();
        expect(await mainPage.spanOpenSelect.getText()).toEqual('Never');
        
        await mainPage.spanOpenSelect.click();
        while(await mainPage.spanOpenSelect.getText() !== '10 Minutes') {
            await browser.keys([Key.ArrowDown]);
            await browser.keys([Key.Enter]);
        }

        expect(await mainPage.spanOpenSelect.getText()).toEqual('10 Minutes');
    });

    it('Paste Name / Title: "helloweb"', async() => {
        await mainPage.pasteNameTitle.setValue('helloweb');
        expect(await mainPage.pasteNameTitle.getValue()).toEqual('helloweb');
    });

})